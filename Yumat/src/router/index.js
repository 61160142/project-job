import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Form from '../views/Form.vue'
import ShowView from '../views/ShowView.vue'
import Table from '../views/Table.vue'
import Counter from '../views/Counter.vue'
import Users from '../views/Users/index.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    path: '/form',
    name: 'Form',
    component: Form
  },
  {
    path: '/showview',
    name: 'ShowView',
    component: ShowView
  },
  {
    path: '/table',
    name: 'Table',
    component: Table
  },
  {
    path: '/counter',
    name: 'Counter',
    component: Counter
  },
  {
    path: '/users',
    name: 'users',
    component: Users
  }
]

const router = new VueRouter({
  routes
})

export default router
